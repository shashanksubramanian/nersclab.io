# Wannier90

[Wannier90](http://www.wannier.org) is a computer package, written in
Fortran90, for obtaining maximally-localised Wannier functions, using
them to calculate bandstructures, Fermi surfaces, dielectric
properties, sparse Hamiltonians and many things besides.

## Availability and Supported Architectures at NERSC

Wannier90 is available at NERSC as a [provided support level](../../policies/software-policy/index.md)
package. Wannier90 runs on CPUs and has MPI-level parallelism.

Use the `module avail` command to see what versions are available, followed
by `module load <version>` to load the module.

```bash
nersc$ module avail wannier90

--------------------- /global/common/software/nersc/pm-2022.12.0/extra_modulefiles ---------------------
   wannier90/3.1.0

nersc$ module load wannier90
```

## Application Information, Documentation, and Support

A user guide, tutorial, documentation are available at the
[support](http://www.wannier.org/support) page. Instructions to
report bugs or errors are also provided there.
For help with issues specific to the NERSC module, please file a
[support ticket](https://help.nersc.gov).

## Using Wannier90 at NERSC

### Examples

See the [example jobs page](../../jobs/examples/index.md) for additional
examples and information about jobs.

!!! tip
	*Only* versions >= 3.0.0 have MPI enabled.
	Versions earlier than 3.0.0 should be run in serial with 
	`srun -n 1 ...`.

!!! tip
	Wannier90 executables compiled using MPI libraries can
	be run in parallel; however, when running in library mode one should
	link only to libraries compiled serially.

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
