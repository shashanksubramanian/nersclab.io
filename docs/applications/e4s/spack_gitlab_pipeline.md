# Spack Gitlab Pipeline 

This guide will discuss how we can leverage spack and gitlab to automate spack builds using continuous integration. If you
are a developer using spack, or member of [Application Development](https://www.exascaleproject.org/research/#application) or
[Software Technology](https://www.exascaleproject.org/research/#software) that wants to install your software product at NERSC, 
you should read this guide. 

Please see https://software.nersc.gov/ecp/e4s-ci-pipelines/dashboard for list of E4S products with nightly builds. If you're 
interested in creating a spack pipeline for your E4S product, please create a ticket in [help desk](https://help.nersc.gov/) 
and mention name of product that you would like to install including any additional members that require access to gitlab 
project. The E4S CI pipelines are located in top-level project https://software.nersc.gov/ecp/e4s-ci-pipelines and each project 
must have a unique name. We recommend project names reflect the product name or name of spack package that you will be building.

There are several use-cases for the nightly build pipelines.

- spack@develop: a build pipeline based on [develop](https://github.com/spack/spack/tree/develop) branch of spack to ensure your 
spack package works with the latest spack codebase.

- spack@branch: a build pipeline based on fixed branch like [e4s-21.05](https://github.com/spack/spack/tree/e4s-21.05) branch or  
 [tag release](https://github.com/spack/spack/tags) of spack 

- Build a spec with unconstrained version such as `hdf5`, fixed version `hdf5@1.10.7`, develop branch `hdf5@develop-1.13`, or 
build with multiple compilers `hdf5%gcc`, `hdf5%intel`. 

## Example Projects 

- [Spack Nightly Build](https://software.nersc.gov/ci-resources/spack-nightly-build): Nightly build for `hdf5` using a fixed 
  version of spack
- [Spack CI Pipeline](https://software.nersc.gov/ci-resources/spack-ci-pipelines): 
[spack pipeline](https://spack.readthedocs.io/en/latest/pipelines.html) based on `spack ci` command
  to parallelize spack builds across multi-stage gitlab job.

## References

- Spack Documentation: https://spack.readthedocs.io/en/latest/

- Gitlab CI Documentation: https://docs.gitlab.com/ee/ci/index.html

- `.gitlab.ci.yml` reference: https://docs.gitlab.com/ee/ci/yaml/
