# Spin

Spin is a container-based platform at NERSC designed for you to
deploy your own science gateways, workflow managers, databases,
API endpoints, and other network services to support your
scientific projects. Services in Spin are built with Docker containers
and can easily access NERSC systems and storage.

For more general information, see the
[Spin web page](https://www.nersc.gov/systems/spin/).

## Accessing Spin

_Users must have an active NERSC account and complete an instructor-led
workshop or self-guided training for access to Spin._

The Rancher system is available at https://rancher2.spin.nersc.gov/ .

The NERSC container image registry, used in conjunction with Rancher, is
available at https://registry.nersc.gov/ .

## Get Started and Get Help

For information about required instructor-led workshops and self-guided
training, see the
[Spin Training & Tutorials page](https://www.nersc.gov/users/training/spin/).

## External Resources

When developing containerized applications, there are several external
resources and communities to draw knowledge from. Here are a few starting
places.

| Resource                                           | Link                                            |
| -------------------------------------------------- | ----------------------------------------------- |
| Docker Container Runtime Documentation             | https://docs.docker.com/reference/              |
| Podman Container Runtime Documentation             | https://docs.podman.io/en/latest/Reference.html |
| Rancher Kubernetes Environment Documentation       | https://ranchermanager.docs.rancher.com/        |
| Kubernetes Container Orchestration Documentation   | https://kubernetes.io/docs/reference/           |
| Helm Documentation (Kubernetes Package Management) | https://helm.sh/docs/                           |
