# GPU Case Studies

NERSC staff along with vendor engineers have worked with
[NESAP](https://www.nersc.gov/research-and-development/nesap/)
applications to optimize codes for the Cori and Perlmutter
architectures. Several of these efforts are documented as case
studies.

* [AMReX GPU Case Study](../amrex-gpu/index.md)
* [DESI GPU Case Study](../desi/index.md)
* [MetaHipMer GPU Case Study](../metahipmer/index.md)
* [Tomopy GPU Case Study](../tomopy/index.md)
* [SNAP GPU Case Study](../snap/index.md)
* [C++ ML Interface](../CPP_2_Py/index.md)
* [Lulesh with C++ Programming Models Case Study](../lulesh-cpp/index.md)
