# Programming Environment Change on Cori in September 2018

## Background

Following the scheduled maintenances on Cori (September 19), we will
install the new Cray Programming Environment Software release
[CDT/18.09](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.09_September_6_2018/resource/Released_Cray_XC_Programming_Environments_18.09_September_6_2018.pdf),
and retire old
[CDT/17.12](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_17.12_December_14_2017/resource/Released_Cray_XC_Programming_Environments_17.12_December_14_2017.pdf).
New Intel compiler version 18.0.3.222 will also be installed. There
will be no software default change this time.

Below is the detailed list of changes after the scheduled maintenance.

## Software versions to be removed

* cray-libsci/17.12.1
* craype/2.5.13
* gcc/7.2.0

## New software versions available

* intel/18.0.3.222
* atp/2.1.3
* cce/8.7.4
* cray-R/3.4.2
* cray-fftw/3.3.8.1
* cray-lgdb/3.0.10
* cray-libsci/18.07.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.3
* cray-netcdf, cray-netcdf-hdf5parallel	4.6.1.3
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.8.4.0
* cray-python/2.7.15.1
* cray-trilinos/12.12.1.1
* gdb4hpc/3.0.10
* papi/5.6.0.3
* perftools, perftools-base, perftools-lite/7.0.3
