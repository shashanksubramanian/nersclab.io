#!/bin/bash
#SBATCH --qos=debug
#SBATCH --nodes=4
#SBATCH --time=10:00
#SBATCH --licenses=cfs,scratch
#SBATCH --constraint=cpu

srun -n 128 -c 8 --cpu_bind=cores ./a.out   
srun -n 64 -c 16 --cpu_bind=cores ./b.out 
srun -n 32 -c 32 --cpu_bind=cores ./c.out
